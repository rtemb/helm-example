package main

import (
	"net/http"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/rtemb/helm-example/internal/api"
	"gitlab.com/rtemb/helm-example/internal/config"
	"gitlab.com/rtemb/helm-example/internal/service"
	"gitlab.com/rtemb/helm-example/pkg/version"
)

func main() {
	logger := logrus.New().WithFields(logrus.Fields{
		"gitSha":  version.GitSha,
		"version": version.ServiceVersion,
		"logger":  "cmd/helm-example",
	})
	logger.Info("loading service configurations")
	cfg, err := config.Load()
	if err != nil {
		logger.Fatal(errors.Wrap(err, "could not load service config"))
	}

	lvl, err := logrus.ParseLevel(cfg.AppConfig.LogLevel)
	if err != nil {
		logger.Fatal(errors.Wrap(err, "could parse log level"))
	}
	logger.Logger.SetLevel(lvl)

	s := service.New(logger, cfg.ServiceLayerConfig)

	a := api.New(logger, s)
	http.HandleFunc("/v1/helm-example/liveness", a.LivenessProbeHandler)
	http.HandleFunc("/v1/helm-example/readiness", a.ReadinessProbeHandler)

	http.HandleFunc("/v1/helm-example/helloworld", a.HelloWorldHandler)

	logger.Info("Running...")
	logger.Fatal(http.ListenAndServe(":"+cfg.AppConfig.ServicePort, nil))
}
