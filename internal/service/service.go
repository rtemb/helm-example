package service

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/rtemb/helm-example/internal/config"
	"time"
)

type HelmExampleService struct {
	logger *logrus.Entry
	cfg    *config.ServiceLayerConfig
}

func New(l *logrus.Entry, cfg *config.ServiceLayerConfig) *HelmExampleService {
	return &HelmExampleService{l, cfg}
}

func (s *HelmExampleService) HelloWorldHandler() string {
	s.logger.WithFields(logrus.Fields{"method": "HelloWorldHandler"})
	//return fmt.Sprintf("Hello world and %s!", s.cfg.HelloName)
	return fmt.Sprintf("%s Hello world and %s!", time.Now().Format(time.RFC3339), s.cfg.HelloName)
}
