package config

import (
	"github.com/kelseyhightower/envconfig"
	"github.com/pkg/errors"
)

type ServiceConfiguration struct {
	AppConfig          *AppConfig
	ServiceLayerConfig *ServiceLayerConfig
}

func Load() (*ServiceConfiguration, error) {
	cfg := &ServiceConfiguration{}
	err := envconfig.Process("", cfg)
	if err != nil {
		return nil, errors.Wrap(err, "unable to parse configuration")
	}

	return cfg, nil
}

type AppConfig struct {
	LogLevel    string `envconfig:"LOG_LEVEL" default:"trace"`
	ServicePort string `envconfig:"SERVICE_PORT" default:"8081"`
}

type ServiceLayerConfig struct {
	HelloName string `envconfig:"HELLO_NAME" default:"Lenin"`
}
