package api

import (
	"encoding/json"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/rtemb/helm-example/internal/service"
)

type HelmExampleApi struct {
	logger  *logrus.Entry
	service *service.HelmExampleService
}

func New(l *logrus.Entry, s *service.HelmExampleService) *HelmExampleApi {
	return &HelmExampleApi{l, s}
}

func (a *HelmExampleApi) LivenessProbeHandler(w http.ResponseWriter, req *http.Request) {
	a.logger.WithFields(logrus.Fields{"method": "api.LivenessProbeHandler"}).Trace(req)
	w.WriteHeader(http.StatusOK)
}

func (a *HelmExampleApi) ReadinessProbeHandler(w http.ResponseWriter, req *http.Request) {
	a.logger.WithFields(logrus.Fields{"method": "api.ReadinessProbeHandler"}).Trace(req)
	w.WriteHeader(http.StatusOK)
}

func (a *HelmExampleApi) HelloWorldHandler(w http.ResponseWriter, req *http.Request) {
	a.logger.WithFields(logrus.Fields{"method": "api.HelloWorldHandler"}).Trace(req)

	res := a.service.HelloWorldHandler()

	rsp := map[string]interface{}{
		"result": res,
	}

	r, err := json.Marshal(rsp)
	if err != nil {
		a.logger.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
	}

	_, _ = w.Write(r)
}
