COVEROUT := coverage.out
APP_NAME := helm-example
APP_VERSION := v0.1.1
OS := linux
#OS := darwin

VERSION_PATH := github.com/rtemb/helm-example/pkg/version.ServiceVersion

.PHONY: deps
deps:
	$(info Installing dependencies)
	GO111MODULE=on go mod download
	GO111MODULE=on go mod vendor

.PHONY: build
build:
	CGO_ENABLED=0 GOOS=$(OS) go build -a -ldflags "-X '$(VERSION_PATH)=$(VERSION)'" -installsuffix cgo -o main ./cmd/...

.PHONY: docker
docker: docker-build docker-push

.PHONY: docker-build
docker-build:
	docker build -t registry.gitlab.com/rtemb/$(APP_NAME):$(APP_VERSION) . --no-cache

.PHONY: docker-push
docker-push:
	docker push registry.gitlab.com/rtemb/$(APP_NAME):$(APP_VERSION)

.PHONY: k8s-commands
k8s-commands:
	minikube stop && minikube delete && minikube start --vm=true --driver=hyperkit
	minikube addons enable ingress
	minikube ip
	nano /etc/hosts

	kubectl create -f k8s-manifest/deployment.yml
	kubectl create -f k8s-manifest/service.yml
	kubectl create -f k8s-manifest/ingress.yml
	kubectl delete ing helmexample-ingress
	kubectl delete deployment helmexample-deployment
	kubectl delete service helmexample-service
	kubectl scale deployment helmexample-deployment --replicas=2

.PHONY: helm-commands
helm-commands:
	helm create helm-example

	helm install helmexample-chart --dry-run --debug ./helm-example
	helm install helmexample-chart ./helm-example
	helm install helmexample-chart --set env.HELLO_NAME="PETYA" ./helm-example

	helm package ./helm-example
	helm install helmexample-chart ./helm-example-0.1.0.tgz
	helm upgrade helmexample-chart helm-example-0.1.1.tgz

	helm get all helmexample-chart

	helm uninstall helmexample-chart

	helm history helmexample-chart
	helm rollback helmexample-chart 1




