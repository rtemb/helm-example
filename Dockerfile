FROM alpine:3.8

ADD main /

EXPOSE 8081

CMD ["./main"]